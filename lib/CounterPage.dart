import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sample_repo/counter_bloc.dart';
import 'package:sample_repo/counter_events.dart';

class CounterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final CounterBloc counterBloc = BlocProvider.of<CounterBloc>(context);
    return Scaffold(
      appBar: AppBar(
          title: Text('Bloc review structure')
      ),
      body: Center(
          child: BlocBuilder<CounterBloc, int>(
            builder: (context, count) {
              return Text('$count', style: TextStyle(fontSize: 24));
            },
          )
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
              onPressed: (){
                counterBloc.add(CounterEvent.increment);
              },
              child: Icon(Icons.add)
          ),
          SizedBox(width: 10),
          FloatingActionButton(
              onPressed: (){
                counterBloc.add(CounterEvent.decrement);
              },
              child: Icon(Icons.remove)
          ),
        ],
      ),
    );
  }
}

